const elem = document.querySelector('.baloon__span');
const words = ['modern', 'fast', 'reliable']; 
let txt = '';
let index = 0;  
let isDeleting = false;
let time = 600;
let currentWord = words[index];


function type () {   

    if (!isDeleting) {
        txt = currentWord.slice(0, txt.length + 1)
        time = 600;
    } else {
        txt = currentWord.slice(0, txt.length - 1) 
        time = 150;               
    }

    if (!isDeleting && txt === currentWord) {
        time = 1500;
        isDeleting = true;
        
    } else if (isDeleting && txt === ''){

        index++;
        if (index > words.length-1) {
            index = 0;
        }
    
        currentWord = words[index];
        isDeleting = false;        
    }
    elem.innerHTML = txt;
    setTimeout(type, time);
}
type();


window.sr = ScrollReveal();
    sr.reveal('.header__logo', {
        duration: 2000,
        origin: 'left',
        distance: '500px'
    });
    sr.reveal('#nav', {
        duration: 2000,
        origin: 'top',
        distance: '500px'
    });

    sr.reveal('.header__contact', {
        duration: 2000,
        origin: 'right',
        distance: '500px'
    });
    
    sr.reveal('.form', {
        duration: 2000,
        delay: 2000,
        origin: 'bottom'        
    });

    sr.reveal('.box1', {
        duration: 2000,        
        origin: 'left',     
        distance: '500px'   
    });

    sr.reveal('.box2', {
        duration: 2000,        
        origin: 'right',
        distance: '500px'      
    });

    sr.reveal('.item', {
        duration: 2000,        
        origin: 'right',
        delay: 1000        
    });

























